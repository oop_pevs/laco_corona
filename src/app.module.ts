import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { BriefModule } from './modules/brief/brief.module';
import { CountriesModule } from './modules/countries/countries.module';
import { TimeseriesModule } from './modules/timeseries/timeseries.module';
import { FinalModule } from "./modules/final/final.module";

@Module({
  imports: [BriefModule, CountriesModule, TimeseriesModule, FinalModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}

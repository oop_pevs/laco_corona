import {Controller, Get, Param, Query} from '@nestjs/common';
import {CoronaDataService} from "../../services/coronaData.service";

@Controller('countries')
export class CountriesController {

    constructor(private coronaDataService: CoronaDataService) {}

    private getDateString(daysAgo = 0): string {
        const date: Date = new Date()
        date.setDate(date.getDate() - daysAgo)

        const d: object = date.toJSON().split('T')[0].split('-')
        return d[1] + '-' + d[2] + '-' + d[0]
    }

    @Get()
    reportsForAllCountries(@Query('date') date: string) {
        date = date || this.getDateString(1)
        const coronaReport = this.coronaDataService.readDailyReportsFile(
            date + '.csv'
        ).then(
            (res) => {
                return res
            }
        )
        return coronaReport
    }

    @Get('/:country')
    reportForOneCountry(
        @Param('country') country: string,
        @Query('date') date: string
    ) {
        date = date || this.getDateString(1)
        const coronaReport = this.coronaDataService.readDailyReportsFile(
            date + '.csv'
        ).then(
            (res) => {
                return res.filter(oneReport => oneReport.countryRegion === country).pop()
            }
        )
        return coronaReport
    }
}

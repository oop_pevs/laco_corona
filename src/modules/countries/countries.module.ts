import { HttpModule, Module } from '@nestjs/common';
import { CountriesController } from './countries.controller';
import { CoronaDataService } from "../../services/coronaData.service";

@Module({
  imports: [HttpModule],
  controllers: [CountriesController],
  providers: [CoronaDataService]
})
export class CountriesModule {}

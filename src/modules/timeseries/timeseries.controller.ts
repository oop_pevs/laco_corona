import {Controller, Get, Query} from '@nestjs/common';
import {CoronaDataService} from "../../services/coronaData.service";

@Controller('timeseries')
export class TimeseriesController {
    constructor(private coronaDataService: CoronaDataService) {}


    @Get()
    timeSeries(@Query('country') country: string) {
        const timeseriesData = this.coronaDataService.readTimeseriesFile(
            'time_series_covid19_confirmed_global.csv'
        ).then(
            (res) => {
                if (country) {
                    return res.filter(item => item.countryRegion == country).pop()
                }
                return res
            }
        )
        return timeseriesData
    }

}

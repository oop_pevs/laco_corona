import {HttpModule, Module} from '@nestjs/common';
import { TimeseriesController } from './timeseries.controller';
import {CoronaDataService} from "../../services/coronaData.service";

@Module({
  imports: [HttpModule],
  controllers: [TimeseriesController],
  providers: [CoronaDataService]
})
export class TimeseriesModule {}

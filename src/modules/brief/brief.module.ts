import {HttpModule, Module} from '@nestjs/common';
import { BriefController } from './brief.controller';
import {CoronaDataService} from "../../services/coronaData.service";

@Module({
  imports: [HttpModule],
  controllers: [BriefController],
  providers: [CoronaDataService]
})
export class BriefModule {}

import {Controller, Get} from '@nestjs/common';
import {CoronaDataService} from "../../services/coronaData.service";

@Controller('brief')
export class BriefController {

    constructor(private coronaDataService: CoronaDataService) {}

    private getDateString(daysAgo = 0): string {
        const date: Date = new Date()
        date.setDate(date.getDate() - daysAgo)

        const d: object = date.toJSON().split('T')[0].split('-')
        return d[1] + '-' + d[2] + '-' + d[0]
    }

    @Get()
    brief() {
        const coronaReport = this.coronaDataService.readDailyReportsFile(
            this.getDateString(1) + '.csv'
        ).then(
            (res) => {
                let confirmed = 0
                let deaths = 0
                let recovered = 0

                res.forEach((oneReport) => {
                    confirmed += oneReport.confirmed
                    recovered += oneReport.recovered
                    deaths += oneReport.deaths
                })

                return {
                    confirmed: confirmed,
                    deaths: deaths,
                    recovered: recovered
                }
            }
        )
        return coronaReport
    }
}

import { Module } from '@nestjs/common';
import { FinalController } from './final.controller';

@Module({
  controllers: [FinalController]
})
export class FinalModule {}

import {HttpService, Injectable} from '@nestjs/common';
import csv = require('csv-parser');

export interface CoronaDailyReport {
    provinceState: string
    countryRegion: string
    lastUpdate: string
    latitude: number
    longitude: number
    confirmed: number
    recovered: number
    deaths: number
}

export interface Timeseries {
    date: string
    value: number
}

export interface CoronaTimeseriesReport {
    provinceState: string
    countryRegion: string
    latitude: number
    longitude: number
    series: Timeseries[]
}

@Injectable()
export class CoronaDataService {
    private TTL = 21600 // 6 hours in seconds
    private tmpPath: string = __dirname + '/../../tmp/'
    private baseDailyReportsUrl = 'https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_daily_reports/'
    private baseTimeseriesUrl = 'https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/'
    private fs

    constructor(private http: HttpService) {
        this.fs = require('fs')
    }

    private parseDailyReportsCsv(fileName: string): Promise<CoronaDailyReport[]> {
        const res: CoronaDailyReport[] = []

        const promise: Promise<CoronaDailyReport[]> = new Promise((resolve, reject) => {

            this.fs.createReadStream(this.tmpPath + fileName)
                .pipe(csv())
                .on('data', (row) => {
                    const dailyReport: CoronaDailyReport = {
                        provinceState: row.Province_State || row['﻿Province/State'],
                        countryRegion: row.Country_Region || row['Country/Region'],
                        lastUpdate: row.Last_Update || row['Last Update'],
                        longitude: Number(row.Longitude) || Number(row.Long_),
                        latitude: Number(row.Latitude) || Number(row.Lat),
                        confirmed: Number(row.Confirmed),
                        recovered: Number(row.Recovered),
                        deaths: Number(row.Deaths),
                    }
                    res.push(dailyReport)
                })
                .on('end', () => {
                    resolve(res)
                })
                .on('error', (e) => {
                    console.log('reading CSV file FAILED')
                    reject(e)
                })
        })
        return promise
    }

    private parseTimeseriesCsv(fileName: string): Promise<CoronaTimeseriesReport[]> {
        const res: CoronaTimeseriesReport[] = []

        const promise: Promise<CoronaTimeseriesReport[]> = new Promise((resolve, reject) => {

            this.fs.createReadStream(this.tmpPath + fileName)
                .pipe(csv())
                .on('data', (row) => {
                    const timeseriesReport: CoronaTimeseriesReport = {
                        provinceState: row['Province/State'],
                        countryRegion: row['Country/Region'],
                        latitude: row['Lat'],
                        longitude: row['Long'],
                        series: []
                    }

                    // Iterate all columns with date
                    for (const [key, value] of Object.entries(row)) {
                        if (key.match('(1[0-2]|[1-9])/([1-9]|[1-2][0-9]|3[0-1])/(20|21)')) {
                            const timeseries: Timeseries = {
                                date: key,
                                value: Number(value)
                            }
                            timeseriesReport.series.push(timeseries)
                        }
                    }

                    res.push(timeseriesReport)
                })
                .on('end', () => {
                    resolve(res)
                })
                .on('error', (e) => {
                    console.log('reading CSV file FAILED')
                    reject(e)
                })
        })
        return promise
    }

    private downloadCsvFile(fileName: string, baseUrl: string) {
        const promise = new Promise((resolve, reject) => {
            this.http.get(baseUrl + fileName, {responseType: "stream"})
                .toPromise()
                .then(response => {
                    const writeStream = this.fs.createWriteStream(this.tmpPath + fileName)
                    response.data.pipe(writeStream)

                    writeStream.on('finish', () => {
                        resolve()
                    }).on('error', (e) => {
                        console.log('Can not write stream to file')
                        reject(e)
                    })
                })
                .catch(() => {
                    console.log('File could not be downloaded')
                    reject()
                })
        })
        return promise
    }

    private isFileExpired(fileName: string): boolean {
        let mtime: number = this.fs.statSync(this.tmpPath + fileName).mtimeMs
        mtime = mtime / 1000 // convert ms to s

        const actualTimestamp: number = Date.now() / 1000 // convert ms to s

        return (actualTimestamp - mtime) > this.TTL
    }

    readDailyReportsFile(fileName: string): Promise<CoronaDailyReport[]> {
        if (
            this.fs.existsSync(this.tmpPath + fileName)
            && !this.isFileExpired(fileName)
        ) {
            return this.parseDailyReportsCsv(fileName)
        }

        return this.downloadCsvFile(fileName, this.baseDailyReportsUrl).then(() => {
            return this.parseDailyReportsCsv(fileName)
        })

    }

    readTimeseriesFile(fileName: string): Promise<CoronaTimeseriesReport[]> {
        if (
            this.fs.existsSync(this.tmpPath + fileName)
            && !this.isFileExpired(fileName)
        ) {
            return this.parseTimeseriesCsv(fileName)
        }

        return this.downloadCsvFile(fileName, this.baseTimeseriesUrl).then(() => {
            return this.parseTimeseriesCsv(fileName)
        })
    }
}

import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

try {
    async function bootstrap() {
        const app = await NestFactory.create(AppModule);
        await app.listen(80);
    }

    bootstrap();
} catch (err) {
    throw new Error(err);
}